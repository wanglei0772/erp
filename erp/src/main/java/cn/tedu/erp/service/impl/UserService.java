package cn.tedu.erp.service.impl;

import cn.tedu.erp.exception.ServiceException;
import cn.tedu.erp.mapper.UserMapper;
import cn.tedu.erp.response.StatusCode;
import cn.tedu.erp.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements IUserService {

    @Autowired
    private UserMapper mapper;

    @Override
    public UserVO login(UserLoginDTO userLoginDTO) {
        UserVO userVO = mapper.selectByUsername(userLoginDTO.getUsername());
        if(userVO==null){
            throw new ServiceException(StatusCode.USERNAME_ERROR);
        }
        if(! userLoginDTO.getPassword().equals(userVO.getPassword())){
            throw new ServiceException(StatusCode.PASSWORD_ERROR);
        }
        return userVO;
    }
}
