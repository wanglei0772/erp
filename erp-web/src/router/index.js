import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from "../views/LoginView.vue";

const routes = [
  {
    path: '/',
    component: LoginView,

  },
  {
    path: '/reg',
    component: ()=>import('../views/RegView')
  },
  {
    path: '/select',
    component: ()=>import('../views/SelectView')
  },
  {
    path: '/detail',
    component: ()=>import('../views/DetailView')
  },

  {
    path: '/home',
    name: 'home',
    component: ()=>import('../views/HomeView')
  }

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
