package cn.tedu.erp.controller;

import cn.tedu.erp.response.JsonResult;
import cn.tedu.erp.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/users")
public class UserController {

    @Autowired
    private IUserService service;

    @RequestMapping("login")
    public JsonResult login(UserLoginDTO userLoginDTO){
        System.out.println("userLoginDTO"+userLoginDTO);
        service.login(userLoginDTO);

        return JsonResult.ok();
    }
}
